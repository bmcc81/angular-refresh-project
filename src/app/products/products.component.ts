import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ProductsService} from '../products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit, OnDestroy {
  productName = 'Cheese';
  // isDisabled = true;
  products = [];
  private productSubscription: Subscription;

  constructor(public productsService: ProductsService) {
    // setTimeout(() => {
    //   this.isDisabled = false;
    // }, 3000);
  }

  ngOnInit() {
    // Load the first time
    this.products = this.productsService.getProducts();

    // Subscribe to update async to return a new array
    this.productSubscription = this.productsService.productsUpdated.subscribe(() => {
      this.products = this.productsService.getProducts();
    });
  }

  ngOnDestroy() {
    this.productSubscription.unsubscribe();
  }

  onAddProduct(form) {
    if (form.valid) {
      this.productsService.addProducts(form.value.productName);
      console.log(form);
      form.controls.productName.value = '';
    }
  }

  onRemoveProduct(productName: string) {
    // filter returns everything except the matching name
    this.products = this.products.filter(product => product !== productName);
  }

}
