import { Subject } from 'rxjs';

export class ProductsService {
  private products = ['A Book'];
  productsUpdated = new Subject();

  addProducts(productName) {
    this.products.push(productName);
    this.productsUpdated.next();
  }

  getProducts() {
    return  [...this.products];
  }

  deleteProduct(productName: string) {
    // Service filters through the products[] and returns the [] except the matching product
    this.products = this.products.filter(product => product !== productName );
    this.productsUpdated.next();
  }

}
