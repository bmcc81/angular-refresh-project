import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {ProductsService} from './products.service';

import {AppComponent} from './app.component';
import {ProductComponent} from './product.component';
import {HomeComponent} from './home.component';
import {ProductsComponent} from './products/products.component';
import { RouterModule } from '@angular/router';

import {AppRoutingModule} from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProductsComponent,
    ProductComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
